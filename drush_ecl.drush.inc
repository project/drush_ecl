<?php

/**
 * @file drush_ecl.drush.inc
 *   drush integration for caching entity content.
 *   Simple idea - load each of the entities.
 */

/**
 * Implementation of hook_drush_help().
 */
function drush_ecl_drush_help($section) {
  switch ($section) {
    case 'drush:entitycache-load':
      return dt('Used without parameters, this command loads all the various content in the entities into cache using entity_load()');
  }
}

/**
 * Implementation of hook_drush_command().
 */
function drush_ecl_drush_command() {
  $items = array();

  $items['entitycache-list'] = array(
    'callback' => 'drush_ecl_entity_list',
    'description' => dt('Get a list of entity information in a summary table.'),
    'arguments' => array(
      'types' => dt('A space separated list of entity types to show.'),
    ),
    'examples' => array(
      'entitycache-list' => 'Displays all entity summaries',
      'entitycache-list node' => 'Displays node entity summary',
      'entitycache-list node user' => 'Displays node and user entity summaries.',
    ),
    'aliases' => array('ec-list'),
  );

  $items['entitycache-load'] = array(
    'callback' => 'drush_ecl_load_cache',
    'description' => dt('Load the cache with the various entities configured to use the cache'),
    'arguments' => array(
      'type' => dt('Optional. Only load the particular entity type objects into cache'),
    ),
    'options' => array(
      'limit' => dt('Set a limit on the number of entities that will get processed per batch run. Defaults to 50'),
      'bundle' => dt('Optionally filter by an entity bundle'),
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('ecl'),
  );

  return $items;
}

/**
 * Display entity information.
 */
function drush_ecl_entity_list() {
  $entities = _drush_ecl_get_entity_info();
  $types = func_get_args();

  if (count($types)) {
    foreach (array_keys($entities) as $entity) {
      if (!in_array($entity, $types)) {
        unset($entities[$entity]);
      }
    }
  }

  $count = count($entities);
  drush_print("\nEntity count: $count\n");

  $header = array(
    dt("Entity"),
    dt("Label"),
    dt("Bundles"),
  );

  // drush etr `drush etr` --fields="bundles/*/label,label,base table,revision table,fieldable,entity class,controller class,drush/count"
  $rows = array(
    $header,
    array('----------'),
  );
  foreach ($entities as $machine => $info) {
    $row = array();
    $bundle_info = array();
    foreach ($info['bundles'] as $bundle) {
      $bundle_info[] = $bundle['label'] . "({$bundle['machine-name']})";
    }
    $row[] = $machine;
    $row[] = isset($info['label']) ? $info['label'] : dt("No label found");
    $row[] = implode("\n", $bundle_info);
    // Add row to rows array.
    $rows[$machine] = $row;
    $rows[] = array('----------');
  }
  drush_print_table($rows, TRUE);
}

/**
 * Load the cache bin with content from the various entities.
 * @param $type Optional. The specific type of entity that should
 *   get it's content cached.
 */
function drush_ecl_load_cache($type = '') {
  $types = entity_get_info();
  if (!empty($type)) {
    if (!isset($types[$type])) {
      drush_die("Type $type is not supported");
    }
    else {
      $types = array($type => $types[$type]);
    }
  }

  // Lets try things in a batch.
  _drush_ecl_batch_load_cache($types);
  $batch =& batch_get();
  $batch['progressive'] = FALSE;
  drush_backend_batch_process();
}

function _drush_ecl_batch_load_cache($types) {
  if (count($types) == 0) {
    return;
  }

  $batch = array(
    'operations' => array(),
    'finished' => '_drush_ecl_batches_finished',
    'title' => dt('Loading cache...'),
    'init_message' => dt('Preparing to cache content...'),
    'progress_message' => dt('Submitting content...'),
    'error_message' => dt('Content could not get cached'),
  );

  $batch_limit = drush_get_option('limit', 50);
  $bundle = drush_get_option('bundle', '');
  foreach ($types as $entity_type => $entity_info) {
    $batch['operations'][] = array(
      'drush_ecl_batch_load_cache', array($entity_type, $entity_info, $bundle, $batch_limit),
    );
  }
  batch_set($batch);
}

/**
 * Batch API entity caching completion message.
 */
function _drush_ecl_batches_finished($success, $results, $operations) {
  drush_print('Succesfully cached all content!');
}

/**
 * Processes and loads cacheable information for entities into cache tables.
 */
function drush_ecl_batch_load_cache($entity_type, $entity_info, $bundle, $batch_limit, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    // Make some assumptions like entity ids must be a positive integer.
    $context['message'] = dt("Begin caching @type", array('@type' => $entity_type));
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_id'] = -1;
    $context['sandbox']['entity_count'] = _drush_ecl_get_entity_count($entity_type, $bundle);
  }
  $current = min($context['sandbox']['progress'] + 1, $context['sandbox']['entity_count']);
  $limit = min($context['sandbox']['progress'] + $batch_limit, $context['sandbox']['entity_count']);
  $type_bundle = empty($bundle) ? $entity_type : "$entity_type.$bundle";
  $context['message'] = dt("Caching @bundle: @current - @limit of @count", array('@bundle' => $type_bundle, '@current' => $current, '@limit' => $limit, '@count' => $context['sandbox']['entity_count']));
  _drush_ecl_load_cache($entity_type, $entity_info, $bundle, $batch_limit, $context);
}

/**
 * Returns total number of entities given a specific entity given.
 */
function _drush_ecl_get_entity_count($entity_type, $bundle) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type);
  if (!empty($bundle)) {
    $query->entityCondition('bundle', $bundle);
  }
  return $query
    ->count()
    ->execute();
}

/**
 * Loads cache bin with content from a specific type of entity.
 */
function _drush_ecl_load_cache($entity_type, $entity_info, $bundle, $batch_limit, &$context) {
  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', $entity_type)
    ->entityCondition('entity_id', $context['sandbox']['current_id'], '>');
  if (!empty($bundle)) {
    $query->entityCondition('bundle', $bundle);
  }
  $result = $query
    ->entityOrderBy('entity_id', 'ASC')
    ->range(0, $batch_limit)
    ->execute();

  $keys = array();
  $limit = 0;
  if (count($result) <= 0) {
    $context['finished'] = TRUE;
    return;
  }
  foreach ($result[$entity_type] as $entity_key => $entity_info) {
    $keys[] = $entity_key;
    $context['sandbox']['progress']++;
    $context['sandbox']['current_id'] = $entity_key;
  }
  _drush_ecl_load_entities($entity_type, $keys);
  // Do a final count to cache any additional entities that may have been added
  // in the interim.
  if ($context['sandbox']['progress'] >= $context['sandbox']['entity_count']) {
    $context['sandbox']['entity_count'] = _drush_ecl_get_entity_count($entity_type, $bundle);
  }
  $context['finished'] = min($context['sandbox']['progress'] / $context['sandbox']['entity_count'], 1.0);
}

/**
 * Load entities provided there are keys.
 */
function _drush_ecl_load_entities($entity_type, $keys = array()) {
  if (count($keys) > 0) {
    entity_load($entity_type, $keys);
  }
}

/**
 * Gets entity information.
 */
function _drush_ecl_get_entity_info() {
  $entities = array();
  $types = entity_get_info();
  foreach ($types as $key => $type) {
    $entity = array(
      'label' => $type['label'],
      'machine-name' => $key,
      'bundles' => array(),
    );
    foreach ($type['bundles'] as $bundle_key => $bundle) {
      $entity['bundles'][] = array(
        'label' => $bundle['label'],
        'machine-name' => $bundle_key,
      );
    }
    $entities[$key] = $entity;
  }
  return $entities;
}
